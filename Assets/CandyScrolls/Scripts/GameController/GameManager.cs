using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager> {
	public GameManager() {}

	/**
	 * GameController manages all players and turns by using data from the Network Controller
	**/
	public GameObject[] players;
	public int activePlayerIndex = -1;

	// Use this for initialization
	void Awake () {
		players = new GameObject[4];
	}

	public void nextPlayerTurn(){
		if(PlayerController.Instance.isMyTurn()){
			PlayerController.Instance.EndMyTurn();
		}

		if(activePlayerIndex < 0 || activePlayerIndex == players.Length - 1 || players[activePlayerIndex + 1] == null){
			activePlayerIndex = 0;
		}
		else{
			activePlayerIndex++;
		}

		if(PlayerController.Instance.isMyTurn()){
			PlayerController.Instance.StartMyTurn();
		}
	}

	public GameObject currentPlayer(){
		if(activePlayerIndex < 0 || activePlayerIndex >= players.Length){
			return null;
		}
		return players[activePlayerIndex];
	}

	public void addPlayer(int playerID, GameObject newPlayer){
		if(players[playerID - 1] != null){
			Debug.Log ("There is already a player in the Game Manager with the index of "+playerID);
		}
		else if(playerID > players.Length){
			Debug.Log ("The player ID provided is greater than the size of the players array");
		}
		else{
			players[playerID - 1] = newPlayer;
		}
	}

	public bool isFull(){
		int playersReady = 0;
		for(int x = 0;x < players.Length; x++){
			if(players[x] != null){
				playersReady++;
			}
		}

		if(playersReady == PhotonNetwork.playerList.Length){
			return true;
		}
		return false;
	}
}
