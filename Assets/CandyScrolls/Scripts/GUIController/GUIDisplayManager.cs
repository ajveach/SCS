﻿using UnityEngine;
using System.Collections;

public class GUIDisplayManager : MonoBehaviour {
	public int playerInfoAreaWidth = 200;
	public int playerInfoAreaHeight = 60;

	public int turnControlsWidth = 400;
	public int turnControlsHeight = 100;

	private PhotonView NetworkControllerPV;

	void Start(){
		GameObject networkController = GameObject.Find ("NetworkController");
		NetworkControllerPV = networkController.GetComponent<PhotonView>();
	}

	void OnGUI(){
		drawPlayerInfoAreas();

		if(PlayerController.Instance.isMyTurn()){
			drawTurnControls();
		}
	}

	// Each player is displayed in a different corner. Start at the top-left and rotate clockwise
	private void drawPlayerInfoAreas(){
		GameObject[] players = GameManager.Instance.players;

		if(players[0] != null){
			// Player 1
			drawPlayerInfoArea (0,0, playerInfoAreaWidth, playerInfoAreaHeight, GameManager.Instance.players[0]);
		}

		if(players[1] != null){
			// Player 2
			drawPlayerInfoArea (Screen.width - playerInfoAreaWidth, 0, playerInfoAreaWidth, playerInfoAreaHeight, GameManager.Instance.players[1]);
		}

		if(players[2] != null){
			// Player 3
			drawPlayerInfoArea (Screen.width - playerInfoAreaWidth, Screen.height - playerInfoAreaHeight, playerInfoAreaWidth, playerInfoAreaHeight, GameManager.Instance.players[2]);
		}

		if(players[3] != null){
			// Player 4
			drawPlayerInfoArea (0, Screen.height - playerInfoAreaHeight, playerInfoAreaWidth, playerInfoAreaHeight, GameManager.Instance.players[3]);
		}
	}

	private void drawPlayerInfoArea(int x, int y, int xMax, int yMax, GameObject player){
		GUI.Box (new Rect(x,y,xMax,yMax),player.name);
		GameObject currentPlayer = GameManager.Instance.currentPlayer();

		GUI.Label (new Rect(x,y+20,xMax,30), player.GetComponent<PlayerData>().GetMarksCount() + " Marks Remaining");

		if(currentPlayer != null && currentPlayer.name == player.name){
			GUI.Label (new Rect(x,y+40,xMax,30), "Active Player");
		}
	}

	private void drawTurnControls(){
		int x = (Screen.width - turnControlsWidth)/2;
		int y = Screen.height - turnControlsHeight - 30;
		GUI.Box (new Rect(x,y,turnControlsWidth,turnControlsHeight),"");

		// End Turn button
		if(GUI.Button (new Rect(x+10,y+10,100,30),"End Turn")){
			if(NetworkControllerPV != null){
				NetworkControllerPV.RPC ("NextTurn",PhotonTargets.All);
			}
		}
	}
}
