﻿using UnityEngine;
using System.Collections;

public class NetworkController : Photon.MonoBehaviour {


	[RPC]
	void StartGame(){
		PhotonNetwork.LoadLevel("Play");
	}

	[RPC]
	void NextTurn(){
		// Tell the local game manager to start the next player's turn
		GameManager.Instance.nextPlayerTurn();
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if(stream.isWriting && photonView.isMine){
			//stream.SendNext(timer);
		}
		else if(!photonView.isMine && stream.isReading){
			//timer = (float)stream.ReceiveNext();
		}
	}

	[RPC]
	void UpdateName(string newName){
		gameObject.name = newName;
	}
}
