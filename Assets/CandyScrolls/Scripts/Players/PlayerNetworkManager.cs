﻿using UnityEngine;
using System.Collections;

public class PlayerNetworkManager : Photon.MonoBehaviour {
	private PlayerData playerData;

	void Start(){
		playerData = GetComponent<PlayerData>();
	}

	[RPC]
	public void AddToGameManager(int playerID, string playerName){
		GameObject player = GameObject.Find (playerName);
		GameManager.Instance.addPlayer(playerID,player);
	}

	[RPC]
	public void UpdateName(string name){
		gameObject.name = name;
	}

	[RPC]
	public void MovePlayer(string hexName){
		GameObject hex = GameObject.Find (hexName);
		if(hex != null){
			gameObject.transform.parent = hex.transform;
			PlayerController.Instance.setPlayerPosition(gameObject.transform,hex.transform);
		}
	}

	[RPC]
	public void AddMark(){
		playerData.AddMark();
	}

	[RPC]
	public void RemoveMark(){
		playerData.RemoveMark();
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){

	}
}
