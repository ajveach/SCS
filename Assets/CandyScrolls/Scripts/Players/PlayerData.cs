﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour {
	public int marks = 3;

	public void AddMark(){
		marks++;
	}

	public void RemoveMark(){
		marks--;
	}

	public int GetMarksCount(){
		return marks;
	}

}
