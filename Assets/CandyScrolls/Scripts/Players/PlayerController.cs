﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : Singleton<PlayerController> {
	protected PlayerController() {}

	public GameObject player;
	public int playerIndex;
	public int playerID;
	public PhotonView playerPhotonView;
	public string currentTurnPhase = null;

	public GameObject cameraController;
	private bool resettingCamera = false;
	public float cameraDistanceX = 0.0f;
	public float cameraDistanceY = 6.0f;
	public float cameraDistanceZ = -6.0f;
	public float cameraResetSpeed = 10.0f;
	private Vector3 cameraResetEndPos;

	private GameObject networkController;
	private PhotonView networkControllerPV;

	private List<GameObject> highlightedHexes;

	void Update(){
		if(resettingCamera){
			// If user is attempting to move the camera then cancel the lerp
			if(Input.GetButtonDown ("Horizontal") || Input.GetButtonDown ("Vertical")){
				resettingCamera = false;
			}
			else{
				cameraController.transform.position = Vector3.Lerp(cameraController.transform.position, cameraResetEndPos, cameraResetSpeed * Time.deltaTime);

				if(Vector3.SqrMagnitude(cameraController.transform.position - cameraResetEndPos) < 0.01){
					resettingCamera = false;
				}
			}
		}
	}

	public void createPlayer(GameObject hexGridGO){
		// Instantiate player and get its photon view component
		player = PhotonNetwork.Instantiate("DemoPlayer",Vector3.zero,Quaternion.identity,0);
		playerPhotonView = player.GetComponent<PhotonView>();

		// Update the player's name around the network
		playerPhotonView.RPC ("UpdateName",PhotonTargets.All,"Player "+playerID);

		cameraController = GameObject.Find ("CameraController");
		int startHex = Random.Range (0,hexGridGO.transform.childCount);
		int currentHex = 0;
		// Place player as child of a hex
		foreach (Transform child in hexGridGO.transform){
			if(currentHex == startHex){
				// Use this hex
				playerPhotonView.RPC ("MovePlayer",PhotonTargets.All,child.gameObject.name);
				break;
			}
			currentHex++;
		}

		this.resetCameraPosition();

		// Add player to game manager
		playerPhotonView.RPC ("AddToGameManager",PhotonTargets.All,playerID,player.name);

		// If the master client, start a co-routine to check to see when all players are ready
		if (PhotonNetwork.isMasterClient){
			StartCoroutine(WaitForAllPlayers());
		}
	}

	IEnumerator WaitForAllPlayers(){
		// TODO: Figure out why this works. It seems backwards.
		bool isFull = GameManager.Instance.isFull ();

		while(isFull == false){
			yield return null;
			isFull = GameManager.Instance.isFull ();
		}

		if(networkController == null){
			this.getNetworkController();
		}
		networkControllerPV.RPC ("NextTurn",PhotonTargets.All);
	}

	public void StartMyTurn(){
		// Start movement phase of turn
		currentTurnPhase = "move";

		// Highlight tiles within moving distance of the player
		highlightedHexes = this.hexesInRange(4);
		foreach(GameObject hex in highlightedHexes){
			hex.GetComponent<GridSelector>().Highlight ();
		}
	}

	public void EndMyTurn(){
		currentTurnPhase = null;

		foreach(GameObject hex in highlightedHexes){
			hex.GetComponent<GridSelector>().RemoveHighlight();
		}

		highlightedHexes = null;
	}

	public void SetPlayerID(int newID){
		playerID = newID;
	}

	public void setPlayerPosition(Transform playerToMove, Transform parentHex){
		Vector3 playerPos = parentHex.position;
		//playerPos.y += playerToMove.localScale.y / 2;
		playerToMove.position = playerPos;
	}

	public void resetCameraPosition(){
		// Have camera center on player
		cameraResetEndPos = new Vector3(player.transform.position.x + cameraDistanceX, player.transform.position.y + cameraDistanceY, player.transform.position.z + cameraDistanceZ);
		resettingCamera = true;
	}

	public bool isMyTurn(){
		if(playerID == GameManager.Instance.activePlayerIndex + 1){
			return true;
		}
		else{
			return false;
		}
	}

	private void getNetworkController(){
		networkController = GameObject.Find ("NetworkController");
		networkControllerPV = networkController.GetComponent<PhotonView>();
	}

	public List<GameObject> hexesInRange(int distance){
		List<GameObject> hexes = new List<GameObject>();
		List<string> hexesChecked = new List<string>();
		List<string> hexesFound = new List<string>();

		hexes.Add(player.transform.parent.gameObject);
		hexesFound.Add (hexes[0].name);

		string[] currentHexName;
		int currentX;
		int currentY;

		Vector2[] posToFind;
		string hexToInspectName;
		GameObject hexToInspect;

		List<GameObject> currentListToCheck = new List<GameObject>();

		// Perform number of neighbor lookups equal to distance
		for(int n = 0; n < distance; n++){
			currentListToCheck.Clear ();
			// All all found hexes that have not been check to list
			foreach(GameObject hex in hexes){
				if(!hexesChecked.Contains (hex.name)){
					currentListToCheck.Add (hex);
				}
			}

			foreach(GameObject hex in currentListToCheck){
				// Add to checked list
				hexesChecked.Add (hex.name);
				currentHexName = hex.name.Split('-');
				currentX = int.Parse (currentHexName[1]);
				currentY = int.Parse (currentHexName[2]);

				posToFind = new Vector2[6];
				if(currentY % 2 == 0){
					// Y is even:
					posToFind[0] = new Vector2(currentX - 1 , currentY - 1);
					posToFind[1] = new Vector2(currentX , currentY - 1 );
					posToFind[2] = new Vector2(currentX + 1 , currentY );
					posToFind[3] = new Vector2(currentX , currentY + 1 );
					posToFind[4] = new Vector2(currentX - 1 , currentY + 1 );
					posToFind[5] = new Vector2(currentX - 1 , currentY );
				}
				else{
					// Y is odd:
					posToFind[0] = new Vector2(currentX , currentY - 1);
					posToFind[1] = new Vector2(currentX + 1 , currentY - 1);
					posToFind[2] = new Vector2(currentX + 1 , currentY);
					posToFind[3] = new Vector2(currentX + 1 , currentY + 1);
					posToFind[4] = new Vector2(currentX , currentY + 1);
					posToFind[5] = new Vector2(currentX - 1 , currentY);
				}

				// Loop through each position, atempt to find the hex and see if it exists. If it exists, place it into the hexes and hexesFound lists.
				foreach(Vector2 pos in posToFind){
					hexToInspectName = "Hex-"+pos.x+"-"+pos.y;
					if(!hexesChecked.Contains (hexToInspectName)){
						// Find GameObject
						hexToInspect = GameObject.Find (hexToInspectName);

						// Check if the Find returned an object, if this object already exists in the found list, and also skip if it has a child object (obstruction)
						if(hexToInspect != null && !hexesFound.Contains (hexToInspectName) && hexToInspect.transform.childCount == 0){
							// Hex was found and it has not already been found. Add to lists
							hexes.Add (hexToInspect);
							hexesFound.Add (hexToInspectName);
						}
					}
				}
			}
		}

		return hexes;
	}
}
