﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuGUI : Photon.MonoBehaviour {	
	public GUIStyle errorStyle;
	public GUIStyle loadingStyle;

	private int lastScreenWidth;
	private int lastScreenHeight;
	
	private int buttonsStartX;
	private int buttonsStartY;

	private bool showLoading = false;

	private List<string> errors = new List<string>();

	private GameObject networkController;

	void Start () {
		PhotonNetwork.ConnectUsingSettings("alpha 0.1");

		// Assign texture background to loadingStyle
		loadingStyle.normal.background = MakeTex(1, 1, new Color(0.0f, 0.0f, 0.0f, 0.5f));

		lastScreenWidth = Screen.width;
		lastScreenHeight = Screen.height;
		
		updateButtonsPosition();
	}
	
	void OnGUI () {
		if(Screen.width != lastScreenWidth || Screen.height != lastScreenHeight){
			updateButtonsPosition();
		}

		int offsetY = 0;

		if(showLoading){
			string loadingMessage;
			if(PhotonNetwork.connectionStateDetailed.ToString() != "Joined"){
				loadingMessage = "Loading... \r\n("+PhotonNetwork.connectionStateDetailed.ToString()+")";
			}
			else{
				loadingMessage = "Waiting for more players... \r\n(Room is ready with "+PhotonNetwork.playerList.Length+" of 4 players waiting)";
			}
			GUI.Box(new Rect(0, 0, Screen.width, Screen.height), loadingMessage, loadingStyle);
		}
		else{
			// Show main menu

			// Print errors
			if(errors.Count > 0){
				foreach(string error in errors){
					GUI.Label (new Rect (buttonsStartX - 150,buttonsStartY + offsetY,400,20), error, errorStyle);
					offsetY += 50;
				}
			}
			
			if (GUI.Button (new Rect (buttonsStartX,buttonsStartY + offsetY,100,20), "Find Game")) {
				this.joinRandomRoom();
			}
			offsetY += 30;
			
			if (GUI.Button (new Rect (buttonsStartX,buttonsStartY + offsetY,100,20), "Host Game")) {
				this.createRoom();
			}
		}
	}

	void Update(){
		if(PhotonNetwork.connectionStateDetailed.ToString() == "Joined" && Input.GetKeyUp(KeyCode.Escape) && PhotonNetwork.isMasterClient && Debug.isDebugBuild){
			// create network controller
			networkController = PhotonNetwork.Instantiate ("NetworkController",Vector3.zero,Quaternion.identity,0);
			PhotonView NCPV = networkController.GetComponent<PhotonView>();
			NCPV.RPC ("UpdateName",PhotonTargets.All, "NetworkController");
			NCPV.RPC ("StartGame",PhotonTargets.All);
		}
	}
	
	void updateButtonsPosition(){
		int buttonsWidth;
		buttonsWidth = 100;
		int buttonsContainerHeight;
		buttonsContainerHeight = 500;
		buttonsStartX = (Screen.width - buttonsWidth)/2;
		buttonsStartY = (Screen.height - buttonsContainerHeight)/2;
	}

	void startLoading(){
		showLoading = true;
	}

	void hideLoading(){
		showLoading = false;
	}

	/**
	 * PHOTON NETWORKING METHODS
	**/
	private void joinRandomRoom(){
		startLoading ();
		PhotonNetwork.JoinRandomRoom();
	}

	private void createRoom(){
		startLoading ();
		PhotonNetwork.CreateRoom("Dev Room", true, true, 4);
	}

	void OnJoinedRoom(){
		// Wait for all players to join
		PlayerController.Instance.SetPlayerID(PhotonNetwork.player.ID);
		//PlayerController.Instance.playerID = PhotonNetwork.player.ID;
	}

	void OnPhotonRandomJoinFailed(){
		errors.RemoveRange (0,errors.Count);
		errors.Add("There are no rooms currently available. Please try to join a room in a moment or create a room.");
		hideLoading();
	}

	/**
	 * GUIStyles METHODS
	**/

	private Texture2D MakeTex(int width, int height, Color col){	
		Color[] pix = new Color[width*height];
		for(int i = 0; i < pix.Length; i++)	
			pix[i] = col;
		Texture2D result = new Texture2D(width, height);
		result.SetPixels(pix);
		result.Apply();
		return result;
		
	}
}
