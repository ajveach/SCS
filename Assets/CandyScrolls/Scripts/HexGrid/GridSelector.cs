﻿using UnityEngine;
using System.Collections;

public class GridSelector : MonoBehaviour {
	private bool isHighlighted = false;

	void OnMouseEnter(){
		if(PlayerController.Instance.isMyTurn()){
			// On player's turn, if in movement phase and mouse is over a valid hex, highlight it further
			if(PlayerController.Instance.currentTurnPhase == "move" && isHighlighted){
				gameObject.renderer.materials[0].color = Color.green;
			}
		}
	}

	void OnMouseExit(){
		if(PlayerController.Instance.isMyTurn()){
			if(isHighlighted){
				gameObject.renderer.materials[0].color = Color.white;
			}
		}
	}

	void OnMouseUp(){
		if(PlayerController.Instance.isMyTurn()){
			if(isHighlighted){
				PlayerController.Instance.playerPhotonView.RPC ("MovePlayer",PhotonTargets.All,gameObject.name);
			}
		}
	}

	public void Highlight(){
		isHighlighted = true;
		gameObject.renderer.materials[0].color = Color.white;
	}

	public void RemoveHighlight(){
		isHighlighted = false;
		gameObject.renderer.materials[0].color = Color.gray;
	}
}
