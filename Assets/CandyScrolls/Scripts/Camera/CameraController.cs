﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public float HScrollSpeed = 5;
	public float VScrollSpeed = 5;

	void Update () {
		float moveH = HScrollSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime;
		float moveV = VScrollSpeed * Input.GetAxis ("Vertical") * Time.deltaTime;

		transform.Translate (moveH,0,moveV);

		if(Input.GetButtonDown ("CenterOnLocalPlayer")){
			PlayerController.Instance.resetCameraPosition();
		}
	}
}
