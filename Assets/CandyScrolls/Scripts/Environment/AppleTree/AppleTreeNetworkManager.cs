﻿using UnityEngine;
using System.Collections;

public class AppleTreeNetworkManager : Photon.MonoBehaviour {

	[RPC]
	void SetHexParent(string hexName){
		GameObject hex = GameObject.Find (hexName);
		if(hex != null){
			transform.parent = hex.transform;
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		
	}
}
