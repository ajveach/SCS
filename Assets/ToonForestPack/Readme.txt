Toon Forest Pack

Thank you for purchasing our Toon Forest Pack. We hope you enjoy our models!

This Package Includes:
- 5 types of stylized trees
- 3 variations for each tree type
- 3 Dry tree trunks
- 3 Rocks
- 3 Mushrooms
- 3 Ground Tillable Textures
- 1 Bonus Christmas Tree!!!

All our textures are 512x512 and include normal, diffuse and specular maps.
Triangle Count ranges from 184 to 904 triangles for the trees.
All objects are set in Prefabs.
Tillable Ground Materials are located in:
ToonForestPack > Meshes > Materials >
Ground-Dirt
Ground-Grass
Ground-Rock
All Textures are inside the Textures Folder. The standard BumpedSpecular Shader uses the Alpha from the Diffuse Texture as a Specular Map. This map is also included as a separate TGA file within the Textures Folder.

Important: Please notice that these Trees are NOT compatible with Tree Maker from Unity. You will have to hand-position every tree. Also, the shader used for the Leaves is a Standard Transparent/Cutout/BumpedSpecular. Depending on your needs, you might have to change this.


Please rate & review our models. It will always help us to improve our quality!


Sincerely,

CatOnTheRoof
contact@catontheroofsw.com